# Ejercicios hechos en C

![alt tag](https://www.cronicasgeek.com/wp-content/uploads/2018/08/LENGUAJE-C.jpg)

Varios ejemplos de código hechos en C (Hechos en Geany, linea de comandos).


## Lista de Ejercicios
|      | Nombre del Ejercicio          | Descripcion   |
|------|-------------------------------|---------------|
| c001 | sumatoria                | Sumatoria de raices.
| c002 | and                      | Aplicacion de AND a valores binarios de a y b.
| c003 | ubicar_bit               | Determinar si el cuarto bit del numero 75 es 0 o 1. 
| c004 | or                       | Se evaluara la operacion or para dor numeros.
| c005 | xor                      | Utilizando xor para determinar su resultado en dos numeros.
| c006 | complementoA1            | Obtener el complemento a 1 de un numero.
| c007 | corrimientoaladerecha    | Hacer corrimiento de bits a la derecha. 
| c008 | corrimientoalaizquierda  | Hacer corrimiento de bits a la izquierda.
| c009 | operadorternario         | Utilizacion del Operador ternario ? : .
| c010 | camposdebits             | Imprime la representacion ascii del caracter en codigo binario.
| c011 | macros                   | Uso basico de macros en c.
| c012 | macro_objeto             | Uso basico de macro objeto.
| c013 | macro_funciones          | Uso basico de macros como funcion.
| c014 | macro_funcion_optimizado | Macro funcion evitando que haya sobre evaluacion de funciones.
| c015 | macrofuncion_parimpar    | Macro funciones evaluando si el numero es par o impar, a bajo nivel.
| c016 | redefinicion_macrofuncion| Redefinicion de una macro-funcion.
| c017 | directiva_undef			| Uso de la directiva undef
| c018 | directivas_if_else_elif	| Uso de la directiva elif
| c019 | directiva_error	    	| Uso de la directiva error.
| c020 | pintarajedrez	    	| Pintar un tablero de ajedrez.
| c021 | factorialrecursivo	   	| Calculo del factorial de un numero recursivamente.
| c022 | Holamundo        	   	| Impresión de texto básico.
| c023 | buscar           	   	| Relleno de array con numeros y la posterior busqueda de uno en particular.
| c024 | array            	   	| Muestr+a los primeros 100 numeros de izquierda a derecha en un array de dos dimenciones.
| c025 | array100          	   	| Rellena un array con los primeros 100 numeros y los muestra en orden ascendente.
| c026 | array100desc        	   	| Rellena un array con los primeros 100 numeros y los muestra en orden descendente.
| c027 | numerosmedia        	   	| Lee 10 numeros, los almacena en un array y calcula la media de estos.
| c028 | numimpares        	   	| Rellena en un array los numeros impares comprendidos entre 1-100 y los muestra en pantalla en orden ascendente.
| c029 | numparessuma       	   	| Rellena un array con los primeros 100 numeros pares y muestra su suma.
| c030 | operaciones       	   	| Lee 10 numeros, los almacena en un array y muestr+a su suma, resta, multiplicacion y division de todos.
| c031 | ordenumeros       	   	| Almacena numeros tanto positivos como negativos y los muestra ordenados.






