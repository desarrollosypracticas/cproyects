/*Creacion de macros como funcion.*/
/*Estos tipos de macros son conocidos como Macro Funciones*/

/*Creando una macro funcion que calcule si un numero es mas
grande que otro.*/

#include <stdio.h>

#define MAX(a,b) ( ( a > b ) ? a : b)

int main(){
		int num1;
		int num2;
		
		printf("Ingrese un numero \n");
		scanf("%d",&num1);
		printf("Ingrese otro numero\n");
		scanf("%d",&num2);
		
		printf("El maximo entre %d y %d es: %d\n",num1,num2,MAX(num1,num2));
		
		return 0;
	}//End main
