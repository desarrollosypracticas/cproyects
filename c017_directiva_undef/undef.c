/*La directiva #undef se utiliza para quitar una definicion de nombre de macro
 * que se haya definido previamente. Es decir, que el objetivo principal de esta 
 * directiva es permitir localizar los nombres de macros solo en las secciones de codigo
 * que se necesiten.*/
 
 //Uso de #undef
 
 #include <stdio.h>
 #define IMPRIMIR(s) \
					printf("Mensaje: \"%s\"\n",s);
					
int main(){
		IMPRIMIR("Hola mundo"); /*Imprime por pantalla Mensaje: Hola Mundo*/
		#undef IMPRIMIR
		
		/*Queremos ahora utilizar IMPRIMIR, dicha macro funcion fue. "eliminada" con lo cual,
		 * */
		IMPRIMIR("funciona?"); /*Verifiquen que esta linea va a producir un error al momento de ejecutarse*/
		return 0;
		
	}//End main
