/*Macro funcion evaluando si un numero es Par o No a bajo nivel.*/

#include <stdio.h>
#define PAR(a) \
		( (a & 1) ? printf("El numero de Impar") : printf("El numero es Par"))
		
int main(){
		int num1;
		
		printf("Ingrese un numero \n");
		scanf("%d",&num1);
		PAR(num1);
		
		return 0;
	}//End main
