/*Hacer un programa que mediante un array almacene numeros tanto positivos como
negativos y los muestre ordenados.*/

#include <stdio.h>
#include <stdlib.h>

int main(void){
	float aux,numeros[10];
	int i,j,n=10;

	for(i=0;i<n;i++){
		printf("Escriba un numero: ");
		scanf("%f",&numeros[i]);
	}/*End for*/

	for(i=0;i<n;i++){
		for(j=0;j<n;j++){
			if(numeros[i]<numeros[j]){
				aux=numeros[i];
				numeros[i]=numeros[j];
				numeros[j]=aux;
			}/*End if*/
		}/*End for2*/
	}/*End for*/

	for(i=n;i>=0;i--){
		printf("%f\n",numeros[i]);
	}/*End for*/

	system("PAUSE");
	return 0;
}/*End main*/
