/*Macro funcion evitando que haya sobre evaluacion de funciones.*/

#include <stdio.h>
#define MAX(a,b) \
				({ typeof (a) _a=(a);	\
					typeof (b) _b = (b); \
					_a > _b ? _a : _b; })
					
int main(){
		int num1;
		int num2;
		
	printf("Ingrese un numero: \n");
	scanf("%d",&num1);
	printf("Ingrese otro numero: \n");
	scanf("%d",&num2);
	
	printf("EL maximo entre %d y %d es: %d \n",num1,num2,MAX(num1,num2));
	
	return 0;
	
	}//End main
