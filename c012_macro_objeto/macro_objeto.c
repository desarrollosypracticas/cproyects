/*Construccion de un macro-objeto.*/
//Se puede apreciar que <nombre del macro> es PI
//<lista de tokens a reemplazar en el codigo> es 3.14159
/*En conclusion se ha creado un Macro Objeto llamado PI, que
 * en cualquier parte del codigo donde aparezca 'PI', sera 
 * reemplazado por 3.14159.*/
 
/*La ventaja de crear este tipo de constantes es que no ocupan
 * lugar en memoria, sino que son reemplazados en tiempo de compilacion,
 * haciendo que el programa sea un poco mas liviano.*/

#include <stdio.h>
#define PI 3.14159
#define SALUDO "Hola Mundo"

int main(){
		printf("%f",PI);// %f para float, imprime 3.14159 por pantalla.
		printf(SALUDO); /*Esto imprime Hola Mundo por pantalla.*/
		
		return 0;
		
		
	}//End main
