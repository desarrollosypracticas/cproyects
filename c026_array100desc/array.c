/*Hacer un programa en C que rellene un array con los 100 primeros números y los muestre en pantalla
en orden descendente.*/

#include <stdio.h>
#include <stdlib.h>

int main(void){
	int x,tabla[100];

	for(x=1;x<=100;x++){
		tabla[x]=x;	
	}/*End for*/

	for(x=100;x>=1;x--){
		printf("%d\n",tabla[x]);
	}/*End for2*/

	system("PAUSE");
	return 0;
}/*End main*/
