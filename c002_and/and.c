
//Aplicacion de AND a valores binarios de a y b
//a=01011010 ; b=10111
//     01011010
//   &
//     00010111
//     --------
//     00010010  = 18
#include <stdio.h>

int main(){
	char a=23;
	char b=90;
	char resultado;
	
	printf("a=%d \nb=%d",a,b);
	resultado=a&b;
	printf("\nResultado=%d",resultado);
	
	return 0;
}//End main
