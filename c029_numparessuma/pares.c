/*Hacer un programa que rellene un array con los 100 primeros numeros pares y muestre
su suma.*/

#include <stdio.h>
#include <stdlib.h>

int main(void){

	int x,cont,sum,i,tabla[100];
	
	i=0;
	sum=0;
	for(x=1;x<=100;x++){
		cont=0;
		if(x%2==0){
			tabla[i]=x;
			i++;
		}/*End if*/
	}/*End for*/

	for(x=0;x<i;x++){
		sum+=tabla[x];
	}/*End for*/

	printf("%d\n",sum);
	
	system("PAUSE");
	return 0;
}/*End main*/
