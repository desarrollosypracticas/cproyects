/*La estructura basica de #error es 
 * 		#error <mensaje_de_error>
 * 
 * Esta directiva forza al compilador a parar la compilacion cuando la 
 * encuentra, mostrando el mensaje de error (<mensaje_de_error>) que se 
 * escribio. Se usa principalmente para depuracion de errores.
 * */

#include <stdio.h>

#define DIVIDIR(a,b) (a/b)
#define NUM1 5
#define NUM2 0

int main(){
		#if NUM2 !=0
			printf("%f",DIVIDIR(NUM1,NUM2));
		#else
			#error division_por_cero!; /*Usualmente utilizado para  muestra de errores en navegadores*/
		#endif
		return 0;
	
	}//End main
