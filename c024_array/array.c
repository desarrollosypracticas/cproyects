/* Hacer un programa que muestre los primeros 100 numeros de izquierda a derecha
usando un array de dos dimensiones. */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]){
	int x,y,numeros[10][10];
	
	for(x=0;x<10;x++){
		for(y=0;y<10;y++){
			numeros[x][y]=(x*10)+1+y;
		}//End for2
	}//End for

	for(x=0;x<10;x++){
		for(y=0;y<10;y++){
			printf("%d ",numeros[x][y]);
		}//End for2
		
		printf("\n");
	
	}//End for

	system("PAUSE");

	return 0;
}//End main
