
/*SI no esta definido el macro _MIARCHIVOCABECERA_H_, lo crea
 * y luego llama el archivo stdio.h*/

#ifndef _MIARCHIVOCABECERA_H_ 
#define _MIARCHIVOCABECERA_H_

#include <stdio.h> /*Incluyo un archivo*/

/*Si ya esta definido el macro _MIARCHIVOCABECERA_H_, incluye mas archivos.*/
#ifdef _MIARCHIVOCABECERA_H_
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#endif /*Fin del #ifdef*/

/*Aca puedo definir tipos de datos*/

/*Aca puedo definir las firmas de funciones/procedimientos.*/

#endif /*fin del #ifndef*/
