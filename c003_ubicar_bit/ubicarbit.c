//Determinar si el cuarto bit del numero 75 es 0 o 1. 

#include <stdio.h>

int main(){
	
		char a=75;
		char b=8;
		char resultado;
		//El cuarto bit se determinara a partir del numero en binario.
		printf("a= %d\nb=%d\n",a,b);
		
		if (a & b)
		{
			printf("El cuarto bit de la variable a es 1\n");
		}else{
			
			printf("El cuarto bit de la variable a es 0\n");
			
			}//End if-else
			
		return 0;

	
	}//End main 
