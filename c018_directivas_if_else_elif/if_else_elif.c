/*La directiva #if evalua una expresion constante entera. Siempre se debe terminar con 
 * #endif para marcar el fin de esta sentencia.
 * 
 * Se pueden asi mismo evaluar otro codigo en caso se cumpla otra condicion, o bien,
 * cuando no se cumple ninguna usando #else o #elif (se puede apreciar que es una 
 * combinacion de #else e #if respectivamente.)*/

#include <stdio.h>

#define LUNES 0
#define MARTES 1

#define DIA_SEMANA LUNES

int main(){
		
		#if DIASEMANA==LUNES
			printf("lunes");
		#elif DIA_SEMANA==MARTES
			printf("MARTES");
		#else
			printf("sera fin de semana ");
		#endif
		
		return 0;
			
	}//End main
