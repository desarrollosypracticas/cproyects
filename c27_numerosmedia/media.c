/*Hacer un programa que lea 10 numeros por teclado, los almacene en un array y muestre
la media.*/

#include <stdio.h>
#include <stdlib.h>

int main(void){
	float sum,numeros1[10];
	int i;

	sum=0;
	for(i=0;i<10;i++){
		printf("Escriba un numero: ");
		scanf("%f",&numeros1[i]);
	}/*End for*/

	for(i=0;i<10;i++){
		sum+=numeros1[i];
	}/*End for*/
	
	printf("%f\n",sum/10);

	system("PAUSE");
	return 0;
}/*End main*/
