#include <stdio.h>
#include <math.h>

int main(void){
    int a,b,i;
    float s;

    /*Pedir limites inferior y superior*/
    printf("Limite inferior");
    scanf("%d",&a);
    while(a<0){
        printf("No puede ser negativo\n");
        printf("Limite inferior:");
        scanf("%d",&a);

    }//End while

    printf("Limite superior:");
    scanf("%d",&b);
    while(b<a){
        printf("No puede ser menor que %d\n",a);
        printf("Limite superior:");
        scanf("%d",&b);

    }//End while

    /*Calcular la sumatoria de la raiz cuadrada de i para i entre a y b*/
    s=0.0;
    for(i=a;i<=b;i++){
        s+=sqrt(i);
    }//End for

    /*Mostrar el resultado*/
    printf("Sumatoria de raices");
    printf("de %d a %d: %f",a,b,s);

}//End main