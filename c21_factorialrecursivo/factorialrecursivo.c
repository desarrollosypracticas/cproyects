#include <stdio.h>

int factorial(int n){
	if(n<=1)
		return 1;
	else
		return n*factorial(n-1);
}

int main (void)
{

	int valor;

	printf("Dame un numero entero positivo:");
	scanf("%d",&valor);
	printf("El factorial de %d vale: %d\n",valor,factorial(valor));
	
	return 0;


}
