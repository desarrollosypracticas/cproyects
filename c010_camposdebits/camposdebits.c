#include <stdio.h>
//Imprime la representacion ascii del caracter en codigo binario
#define mayus(ch) ((ch>='a' && ch<='z') ? (ch+'A'-'a') : ch)

typedef struct{
    unsigned int a: 1;
    unsigned int b: 1;
    unsigned int c: 1;
    unsigned int d: 1;
    unsigned int e: 1;
    unsigned int f: 1;
    unsigned int g: 1;
    unsigned int h: 1;
}byte;

union charbits{
    char ch;
    byte bits;
}caracter;

void decodifica(byte b);

int main(void){
    printf("\n\n ----- Teclea caracteres. Para salir car%ccter X -----\n\n", 160);
    
    do {
        caracter.ch = getchar();
        printf (" : ") ;

        decodifica(caracter.bits);

    }while(mayus(caracter.ch) !='X');

    return 0;
}

void decodifica(byte b){
    printf("%2u%2u%2u%2u%2u%2u%2u%2u \n", b.h, b.g, b.f, b.e, b.d, b.c, b.b, b.a);
}
