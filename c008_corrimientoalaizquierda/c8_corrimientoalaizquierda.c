//	a=01101001 equivalente a 105
//  Sus corrimientos equivaldrian a: 
//										01101001	1	11010010	210
//										01101001	2	10100100	420
//										01101001	3	01001000	840
//										01101001	4	10010000	1680
//										01101001	5	00100000	3360
// Para aplicarlo seria: 	numero << cantidad de bits de corrimiento

#include <stdio.h>

int main(){
		
		int a=105;
		int resultado;
		
		printf("a=%d\n",a);
		
		resultado=a<<1;
		
		printf("Resultado=%d\n",resultado);	/*Imprime 210*/
		
		resultado=a<<2;
		
		printf("Resultado=%d\n",resultado);	/*Imprime 420*/
		
		return 0;
		
	}//End main
