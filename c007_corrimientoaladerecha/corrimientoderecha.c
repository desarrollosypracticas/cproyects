//Hacer corrimiento de bits a la derecha

#include <stdio.h>

int main(){
	
	int a=105;
	int resultado;
	
	printf("a=%d",a);
	
	resultado=a>>1; //Hace el corrimiento de 1 bit
	
	printf("Resultado=%d",resultado); /*imprime 52*/
	
	resultado=a>>2; //Hace el corrimiento de 2 bits
	
	printf("Resultado=%d",resultado); /*imprime 26*/
	
	return 0;
	
	
	
	
	}//End main
